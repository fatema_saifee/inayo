# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-12 12:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0013_auto_20160906_0741'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ordereditem',
            name='stock',
        ),
        migrations.RemoveField(
            model_name='ordereditem',
            name='stock_location',
        ),
        migrations.RemoveField(
            model_name='ordereditem',
            name='unit_price_gross',
        ),
        migrations.AlterField(
            model_name='ordereditem',
            name='product',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='product'),
        ),
    ]
