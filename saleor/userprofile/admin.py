from django.contrib import admin
from django.db.models import Q

from .models import User

class UserAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "managed_by":
            kwargs["queryset"] = User.objects.filter(Q(is_manager=True) | Q(is_store_manager=True))
        return super(UserAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(User, UserAdmin)
