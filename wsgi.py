"""
WSGI config for Inayo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append('/root/inayo/source/inayo')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "saleor.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

os.environ["SECRET_KEY"] = "p^_w%pyw(rj&g&2imo&4fw8nz5a8ix+@y1avdmu_q+3_+3+-t7"
