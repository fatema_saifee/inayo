# from __future__ import unicode_literals

from django.http import HttpResponsePermanentRedirect
from django.conf import settings
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
# from .forms import get_form_class_for_product
# from .models import Product, Category
# from ..cart import Cart
from saleor.core.utils import get_paginator_items
# from sunburnt import SolrInterface
import httplib2
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
from django.http import HttpResponse
import urllib
from babeldjango.templatetags.babel import currencyfmt

from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _

from .models import Cart
from .forms import ReplaceCartLineForm, AddToCartForm
# from ..cart.utils import (
#     contains_unavailable_products, remove_unavailable_products)


def cart_index(request, product_id=None):
    # if product_id is not None:
    #     product_id = int(product_id)
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    # if contains_unavailable_products(cart):
    #     msg = _('Sorry. We don\'t have that many items in stock. '
    #             'Quantity was set to maximum available for now.')
    #     messages.warning(request, msg)
    #     remove_unavailable_products(cart)
    for line in cart:
        data = None
        # import pdb; pdb.set_trace()
        if line.product['_id'] == product_id:
            data = request.POST
        initial = {'quantity': line.get_quantity()}
        form = ReplaceCartLineForm(data, cart=cart, product=line.product,
                                   initial=initial)
        line.form = form
        if form.is_valid():
            form.save()
            if request.is_ajax():
                response = {
                    'productId': line.product['_id'],
                    'subtotal': currencyfmt(
                        line.get_total().gross,
                        line.get_total().currency),
                    'total': 0}
                if cart:
                    response['total'] = currencyfmt(
                        cart.get_total().gross, cart.get_total().currency)
                return JsonResponse(response)
            return redirect('inayo:cart')
        elif data is not None:
            if request.is_ajax():
                response = {'error': form.errors}
                return JsonResponse(response, status=400)

    return TemplateResponse(
        request, 'inayo/data_center/other_product/cart.html', {
            'cart': cart})


@login_required
def index(request):#, path, category_id):
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/healthkart/select?q=*:*&rows=50&wt=python')
    response = eval(connection.read())['response']['docs']
    form_class = AddToCartForm
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    form = form_class(cart=cart, product=response,
                      data=request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponse(json.dumps({'message': 'success'}))
        return HttpResponse(json.dumps({'message': 'failure'}))
    for r in response:
        try:
            r['id'] = r['_id']
        except:
            r['id'] = "acjjk4398unkns"
        cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
        form = form_class(cart=cart, product=r,
                          data=request.POST or None)
        r['form'] = form
    return TemplateResponse(
        request, 'inayo/data_center/other_product/index.html',{'response': response, 'form': form})

def product_detail(request, p_id):
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/healthkart/select?q=_id:'+p_id+'&wt=json')
    # import pdb; pdb.set_trace()
    response = eval(connection.read())['response']['docs'][0]
    form_class = AddToCartForm
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    form = form_class(cart=cart, product=response,
                      data=request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('other_product:index')

    template_name = 'inayo/data_center/other_product/details.html'
    templates = [template_name, 'inayo/data_center/other_product/details.html']
    return TemplateResponse(
        request, templates,
        {'response': response, 'form': form})
    # return TemplateResponse(
    #     request, 'inayo/data_center/details.html',{'response': response})

def get_next_page(request, page_number):
    start = (int(page_number)-1)*10
    end = int(page_number)*10
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/healthkart/select?q=*:*&start='+str(start)+'&rows='+str(end)+'&wt=json')
    response = connection.read()
    return HttpResponse(response, content_type="text/json")


def search_index(request, page_number):
    start = (int(page_number)-1)*10
    end = int(page_number)*10

    url = "http://localhost:8983/solr/healthkart/select?rows=20&wt=json&q=" + urllib.parse.quote(request.GET['url'])
    # import pdb; pdb.set_trace();
    connection = urllib2.urlopen(url+'&start='+str(start)+'&rows='+str(end))
    response = connection.read()
    return HttpResponse(response, content_type="text/json")

def add_form_save(request, p_id, quantity):
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/healthkart/select?q=_id:'+p_id+'&wt=json')
    # import pdb; pdb.set_trace()
    product = eval(connection.read())['response']['docs'][0]
    if product:
        cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
        cart.add(product, int(quantity))
    return HttpResponse({"response":"success"},content_type="text/json")
