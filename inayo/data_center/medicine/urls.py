from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^/(?P<page_number>[0-9]+)/$',
        views.get_next_page, name='details'),
    url(r'/details/(?P<p_id>\w+)/$',
        views.product_detail, name='product_detail'),
    url(r'^/cart/$', views.cart_index, name='cart'),
    url(r'^/search/(?P<page_number>[0-9]+)/*', views.search_index, name='search'),
    url(r'^/submit_form/(?P<p_id>\w+)/(?P<quantity>\d+)/',views.add_form_save, name='submit_form')
]
