# from __future__ import unicode_literals

from django.http import HttpResponsePermanentRedirect
from django.conf import settings
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from saleor.core.utils import get_paginator_items
import httplib2
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
from django.http import HttpResponse

from babeldjango.templatetags.babel import currencyfmt

from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _

from .models import Cart
from .forms import ReplaceCartLineForm, AddToCartForm


def cart_index(request, product_id=None):
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    for line in cart:
        data = None
        # import pdb; pdb.set_trace()
        if line.product['_id'] == product_id:
            data = request.POST
        initial = {'quantity': line.get_quantity()}
        form = ReplaceCartLineForm(data, cart=cart, product=line.product,
                                   initial=initial)
        line.form = form
        if form.is_valid():
            form.save()
            if request.is_ajax():
                response = {
                    'productId': line.product['_id'],
                    'subtotal': currencyfmt(
                        line.get_total().gross,
                        line.get_total().currency),
                    'total': 0}
                if cart:
                    response['total'] = currencyfmt(
                        cart.get_total().gross, cart.get_total().currency)
                return JsonResponse(response)
            return redirect('inayo:cart')
        elif data is not None:
            if request.is_ajax():
                response = {'error': form.errors}
                return JsonResponse(response, status=400)

    return TemplateResponse(
        request, 'inayo/data_center/cart.html', {
            'cart': cart})


@login_required
def index(request):
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/inayo/select?q=*:*&rows=50&wt=python')
    response = eval(connection.read())['response']['docs']
    form_class = AddToCartForm
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    form = form_class(cart=cart, product=response,
                      data=request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponse(json.dumps({'message': 'success'}))
        return HttpResponse(json.dumps({'message': 'failure'}))
    for r in response:
        r['id'] = r['_id']
        cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
        form = form_class(cart=cart, product=r,
                          data=request.POST or None)
        r['form'] = form
    return TemplateResponse(
        request, 'inayo/data_center/index.html',{'response': response, 'form': form})

def product_detail(request, p_id):
    '''
        get product detail page for the selected product
    '''
    form_class = AddToCartForm
    cart = Cart.for_session_cart(request.cart, discounts=request.discounts)
    form = form_class(cart=cart, product=response,
                      data=request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('inayo:index')

    connection = urllib2.urlopen(
                'http://localhost:8983/solr/inayo/select?q=_id:'+p_id+'&wt=json')
    response = eval(connection.read())['response']['docs'][0]
    template_name = 'inayo/data_center/details.html'
    templates = [template_name, 'inayo/data_center/details.html']
    return TemplateResponse(
        request, templates,
        {'response': response, 'form': form})

def get_next_page(request, page_number):
    '''
        get next 10 products from solr for the ajax request
    '''
    start = (int(page_number)-1)*10
    end = int(page_number)*10
    connection = urllib2.urlopen(
                'http://localhost:8983/solr/inayo/select?q=*:*&start='+str(start)+'&rows='+str(end)+'&wt=json')
    response = connection.read()
    return HttpResponse(response, content_type="text/json")
