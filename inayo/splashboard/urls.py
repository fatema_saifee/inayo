from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^orders$', views.orders, name='orders'),
    url(r'^managers$', views.managers, name='managers'),
    url(r'^stores$', views.stores, name='stores'),
    url(r'^employees$', views.employees, name='employees'),
    # url(r'^(?P<pk>[0-9]+)/$', views.customer_details, name='customer-details')
]
