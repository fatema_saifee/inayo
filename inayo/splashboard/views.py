import datetime
from django.conf import settings
from django.contrib.admin.views.decorators import \
    staff_member_required as _staff_member_required
from django.db.models import Q, Sum
from django.template.response import TemplateResponse
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormMixin
from django.db.models import Count, Sum, F, FloatField
from django.db.models.functions import TruncMonth

from saleor.order.models import Order, Payment
# from saleor.product.models import Product
from saleor.dashboard.order.forms import OrderFilterForm
from saleor.userprofile.models import User
from inayo.splashboard.forms import UserForm
from django.shortcuts import redirect

def staff_member_required(f):
    return _staff_member_required(f, login_url='registration:login')


@staff_member_required
def index(request):

    if not (request.user.is_store_manager or request.user.is_store_manager):
        return redirect('/medicine')

    monthly_orders = Order.objects.annotate(month=TruncMonth('created')).values('month').annotate(c=Sum(F('total_net'))).values('month', 'c')
    order = Order.objects.extra(select={'month': 'extract( month from created )'}).values('month').annotate(dcount=Count('created'))

    last_month_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=30))
    last_year_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=365))
    last_week_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=7))
    last_quater_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=120))
    last_five_year_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=1825))
    last_six_months_orders =  Order.objects.filter(created__lte=datetime.datetime.today(), created__gt=datetime.datetime.today()-datetime.timedelta(days=182))
    orders = Order.objects.all()
    user = request.user
    ten_stores = User.objects.all()[:10]
    ten_orders = Order.objects.all()[:10]
    ctx = {'orders' : orders,
            'last_month_orders':last_month_orders,
            'last_year_orders':last_year_orders,
            'last_week_orders':last_week_orders,
            'last_quater_orders':last_quater_orders,
            'last_five_year_orders':last_five_year_orders,
            'last_six_months_orders':last_six_months_orders,
            'ten_stores':ten_stores,
            'ten_orders':ten_orders}
    if user.is_active:
        return TemplateResponse(request, 'inayo/dashboard/index.html', ctx)

@staff_member_required
def orders(request):
    if request.user.is_store_manager:
        orders = Order.objects.filter(user=request.user)
    orders = Order.objects.all()
    user = request.user
    ctx = {'orders' : orders}
    if user.is_active:
        return TemplateResponse(request, 'inayo/dashboard/orders.html', ctx)

@staff_member_required
def managers(request):
    managers = User.objects.filter(is_manager = True)
    user = request.user
    ctx = {'managers':managers}
    if user.is_active:
        return TemplateResponse(request, 'inayo/dashboard/managers.html', ctx)

@staff_member_required
def stores(request):
    '''
        add a new store.
    '''
    if request.method == 'POST': # If the form has been submitted...
        # ContactForm was defined in the previous section
        form = UserForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            new_user = form.save(commit=False)
            new_user.is_staff = True
            new_user.is_active = True
            form.save()
    else:
        form = UserForm() 

    stores = User.objects.all()
    user = request.user
    ctx = {'stores':stores,
        'form': form,
    }
    if user.is_active:
        return TemplateResponse(request, 'inayo/dashboard/stores.html', ctx)

@staff_member_required
def employees(request):
    employees = User.objects.filter(managed_by=request.user)
    user = request.user
    ctx = {'employees':employees}
    if user.is_active:
        return TemplateResponse(request, 'inayo/dashboard/employees.html', ctx)
