# from django import forms
#
# class CreateUserForm(forms.Form):
#     username = forms.CharField(max_length=30)
#     first_name = forms.CharField()
#     last_name = forms.CharField()
#     password1=forms.CharField(max_length=30,widget=forms.PasswordInput()) #render_value=False
#     password2=forms.CharField(max_length=30,widget=forms.PasswordInput())
#     email=forms.EmailField(required=False)
#
#     title = forms.ChoiceField(choices=TITLE_CHOICES)
#
#     def clean_username(self): # check if username dos not exist before
#         try:
#             User.objects.get(username=self.cleaned_data['username']) #get user from user model
#         except User.DoesNotExist :
#             return self.cleaned_data['username']
#
#         raise forms.ValidationError("this user exist already")
#
#
#     def clean(self): # check if password 1 and password2 match each other
#         if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:#check if both pass first validation
#             if self.cleaned_data['password1'] != self.cleaned_data['password2']: # check if they match each other
#                 raise forms.ValidationError("passwords dont match each other")
#
#         return self.cleaned_data
#
#
#     def save(self): # create new user
#         new_user=User.objects.create_user(self.cleaned_data['username'],
#                                           self.cleaned_data['email'],
#                                           self.cleaned_data['password1'])
#         new_user.first_name = self.cleaned_data['first_name']
#         new_user.last_name = self.cleaned_data['last_name']
#         new_user.save()
from django.db import models
from django.forms import ModelForm
from saleor.userprofile.models import User
from django.core.exceptions import NON_FIELD_ERRORS

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password','managed_by']
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }
