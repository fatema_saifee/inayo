from django.template.response import TemplateResponse
from django.shortcuts import redirect

def home(request):
    if request.user.is_authenticated():
        return redirect('/medicine')
    return TemplateResponse(
        request, 'inayo/home.html')
